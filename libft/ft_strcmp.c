/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 12:51:31 by vjacquie          #+#    #+#             */
/*   Updated: 2015/01/26 19:44:52 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

int	ft_strcmp(const char *s1, const char *s2)
{
	int				i;
	unsigned char	one;
	unsigned char	two;

	i = 0;
	one = s1[0];
	two = s2[0];
	while (s1[i] != '\0' || s2[i] != '\0')
	{
		one = s1[i];
		two = s2[i];
		if (one == two)
			i++;
		else
			return (one - two);
	}
	return (one - two);
}
