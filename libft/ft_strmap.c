/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 19:04:52 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:34:37 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	size_t	length;
	char	*s1;

	i = 0;
	length = ft_strlen(s);
	s1 = (char *)malloc(sizeof(char) * (length + 1));
	ft_memcpy(s1, s, length + 1);
	while (s[i])
	{
		s1[i] = f(s[i]);
		i++;
	}
	return (s1);
}
