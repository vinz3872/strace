/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 15:01:04 by vjacquie          #+#    #+#             */
/*   Updated: 2015/01/26 19:46:14 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	index;
	char			*s1;
	size_t			length;

	index = 0;
	length = ft_strlen(s);
	s1 = (char *)malloc(sizeof(char) * (length + 1));
	ft_memcpy(s1, s, length + 1);
	while (s[index])
	{
		s1[index] = f(index, s[index]);
		index++;
	}
	return (s1);
}
