/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/20 15:41:09 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 18:16:53 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

int		getRegisterArgByNumber(int nbr) {
	if (nbr == 0)
		return (RDI);
	else if (nbr == 1)
		return (RSI);
	else if (nbr == 2)
		return (RDX);
	else if (nbr == 3)
		return (R10);
	else if (nbr == 4)
		return (R8);
	else //(nbr == 5)
		return (R9);
}

void	print_arg_int(int reg, pid_t pid) {
	size_t value = ptrace(PTRACE_PEEKUSER, pid, sizeof(long) * reg);
	printf("%d", (int)value);
}

void	print_arg_str(int reg, pid_t pid) {
	unsigned long addr = ptrace(PTRACE_PEEKUSER, pid, sizeof(long) * reg);
	char *str = malloc(4096);
	int allocated = 4096;
	int read = 0;
	unsigned long tmp;
	while (1) {
		if (read + sizeof tmp > (size_t)allocated) {
			allocated *= 2;
			str = realloc(str, allocated);
		}
		tmp = ptrace(PTRACE_PEEKDATA, pid, addr + read);
		if(errno != 0) {
			str[read] = 0;
			break;
		}
		memcpy(str + read, &tmp, sizeof tmp);
		if (memchr(&tmp, 0, sizeof tmp) != NULL)
			break;
		read += sizeof tmp;
	}
	printf("\"%.65s\"", str);
	if (strlen(str) > 65)
		printf("...");
	free(str);
}

void	print_arg_ptr(int reg, pid_t pid) {
	size_t value = ptrace(PTRACE_PEEKUSER, pid, sizeof(long) * reg);
	printf("%p", (void *)value);

}
