/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 12:39:28 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:34:06 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strcat(char *s1, const char *s2)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (s1[i])
	{
		i = i + 1;
	}
	while (s2[j])
	{
		s1[i] = s2[j];
		j = j + 1;
		i = i + 1;
	}
	s1[i] = '\0';
	return ((char *)&s1[0]);
}
