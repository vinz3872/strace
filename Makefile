# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/04 17:41:14 by vjacquie          #+#    #+#              #
#    Updated: 2016/05/20 19:00:41 by vjacquie         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc -Wall -Wextra -Werror

RM = rm -rf

INCLUDES = ./includes

LIB = libft/libft.a

HEADER = includes/ft_strace.h

NAME = ft_strace

SRCS =  \
		src/main.c \
		src/parse.c \
		src/print_error.c \
		src/lst.c \
		src/data.c \
		src/print_all.c \
		src/print_arg.c \
		src/print_retval.c \
		src/signal.c

OBJS = $(SRCS:.c=.o)

%.o: %.c $(HEADER)
	$(CC) $(SRC) -I $(INCLUDES) -c $< -o $@

all : $(NAME)

$(LIB):
	make -C libft

$(NAME): $(LIB) $(OBJS)
	$(CC) $(OBJS) -o $(NAME) $(LIB)

clean:
	make clean -C libft
	$(RM) $(OBJS)

fclean: clean
	make fclean -C libft
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
