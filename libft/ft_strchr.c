/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 14:55:05 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:34:08 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;
	char	d;

	i = 0;
	d = c;
	while ((s[i] != '\0') && (s[i] != d))
	{
		i = i + 1;
	}
	if (s[i] == d)
		return ((char *)(&s[i]));
	return (NULL);
}
