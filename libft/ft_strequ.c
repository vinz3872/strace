/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 15:06:52 by vjacquie          #+#    #+#             */
/*   Updated: 2014/04/24 15:34:21 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/libft.h"

int	ft_strequ(char const *s1, char const *s2)
{
	int	number;

	number = 0;
	while ((s1[number] && s2[number]) && ((s1[number] - s2[number]) == 0))
	{
		number++;
		if ((s1[number] != s2[number]) || (s1[number] - s2[number]) != 0)
			return (0);
	}
	if (((s1[number] || s2[number]) && !(s1[number] && s2[number]))
		|| ((s1[number] - s2[number]) != 0))
		return (0);
	else
		return (1);
}
