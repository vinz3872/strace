/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 12:55:39 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/17 14:12:54 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

t_syscall	*newLst() {
	t_syscall	*new = malloc(sizeof(t_syscall));

	assert(new != NULL);
	new->name = NULL;
	new->sysNumber = -1;
	new->argNbr = -1;
	memset(new->type, -1, sizeof(new->type));
	new->next = NULL;

	return (new);
}

void		addLst(t_syscall *new) {
	t_syscall	*tmp;
	t_data *d = getData();

	if (d->sysLst == NULL) {
		d->sysLst = new;
		return ;
	}
	tmp = d->sysLst;
	while (tmp->next != NULL) {
		tmp = tmp->next;
	}
	tmp->next = new;
}
