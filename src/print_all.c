/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_all.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 14:38:27 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 18:58:59 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

void	printOther(int num) {
	printf("syscall %d", num);
}

void	printAll(int num) {
	t_data		*d = getData();
	t_syscall	*tmp = d->sysLst;
	int			i = 0;
	size_t		retval = 0;

	while (tmp != NULL) {
		if (tmp->sysNumber == num)
			break;
		tmp = tmp->next;
	}
	if (tmp == NULL)
		printOther(num);
	else {
		printf("%s(", tmp->name);
		while (i < tmp->argNbr) {
			if (tmp->type[i] == ARG_INT)
				print_arg_int(getRegisterArgByNumber(i), d->pid);
			else if (tmp->type[i] == ARG_STR)
				print_arg_str(getRegisterArgByNumber(i), d->pid);
			else if (tmp->type[i] == ARG_PTR)
				print_arg_ptr(getRegisterArgByNumber(i), d->pid);
			i++;
			if (i < tmp->argNbr)
				printf(", ");
		}
		retval = ptrace(PTRACE_PEEKUSER, d->pid, sizeof(long) * RAX);
		if ((int)retval < -5 || (int)retval > 100000)
			print_retval_ptr(retval);
		else
			printf(")\t= %d\n", (int)retval);
	}
}
