/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/24 13:11:02 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 18:52:21 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

int wait_for_syscall(pid_t pid) {
    int status;
    while (1) {
        ptrace(PTRACE_SYSCALL, pid, 0, 0);
        waitpid(pid, &status, 0);
        if (WIFSTOPPED(status) && WSTOPSIG(status) & 0x80)
            return (0);
        if (WIFEXITED(status)) {
			printf("exit_group(%d)\n", WEXITSTATUS(status));
			return (1);
		}
		if (status>>8 == (SIGTRAP | (PTRACE_EVENT_EXEC<<8)))
			getData()->begin = 1;
		else
        	fprintf(stderr, "[stopped %d (%x)]\n", status, WSTOPSIG(status));
    }
}

int	do_syscall_loop(pid_t pid) {
	t_data	*d = getData();
	d->pid = pid;

	setSignal();
	while (1) {
		if (wait_for_syscall(pid) != 0)
			break;
        int syscall = ptrace(PTRACE_PEEKUSER, pid, sizeof(long) * ORIG_EAX);
        if (wait_for_syscall(pid) != 0)
			break;
		if (d->begin == 1)
			printAll(syscall);
	}
	return (0);
}

int	do_trace(pid_t pid) {
	int status;
	unsigned int ptrace_setoptions = PTRACE_O_TRACESYSGOOD | PTRACE_O_TRACEEXEC;
	printf("pid %d\n", pid);
	if (ptrace(PTRACE_SEIZE, pid, 0, ptrace_setoptions) != 0)
		fprintf(stderr, "PTRACE_SEIZE doesn't work\n");
	waitpid(pid, &status, 0);
	assert(WIFSTOPPED(status));

	do_syscall_loop(pid);

	ptrace(PTRACE_DETACH, pid, 0, 0);
	return (0);
}

int do_child(int ac, char **av) {
	char *cpy[ac];
	int i = 1;

	while (i < ac) {
		cpy[i - 1] = av[i];
		i++;
	}
	cpy[i - 1] = NULL;
	kill(getpid(), SIGSTOP);
	return (execvp(cpy[0], cpy));
}

int do_fork(int ac, char **av) {
	pid_t child;

	child = fork();
	if (child == 0)
		return do_child(ac, av);
	else
		return do_trace(child);
}

int	main(int ac, char **av) {
    getData();
    parseFile();
    if (ac < 2)
		fprintf(stderr, "%s", USAGE);
	else
		return do_fork(ac, av);
	return (0);
}
