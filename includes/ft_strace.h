/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strace.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/24 13:15:05 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 18:50:00 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STRACE_H
# define FT_STRACE_H

# include "../libft/includes/libft.h"

# include <sys/ptrace.h>
# include <sys/reg.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <errno.h>
# include <string.h>
# include <sys/stat.h>
# include <fcntl.h>

# include <sys/user.h>
# include <assert.h>

# define USAGE "Usage: ./ft_strace <program> [<args>]\n"
# define ORIG_EAX ORIG_RAX
# define SYSCALLPATH "syscallData"

enum    sysType {
    ARG_INT,
    ARG_STR,
	ARG_PTR
};

typedef struct  s_syscall {
    char				*name;
    int					sysNumber;
    int					argNbr;
	int					type[6];

    struct s_syscall	*next;
}               t_syscall;

typedef struct	s_data {
	t_syscall	*sysLst;
	int			begin;
	pid_t		pid;
	// struct user_regs_struct	regs;
}				t_data;

int		print_error(char *str);
int		parseFile();
t_data	*getData();
void		addLst(t_syscall *new);
t_syscall	*newLst();
void	printAll(int num);

void	print_arg_int(int reg, pid_t pid);
void	print_arg_str(int reg, pid_t pid);
void	print_arg_ptr(int reg, pid_t pid);
int		getRegisterArgByNumber(int nbr);
void	print_retval_ptr(size_t val);
void	setSignal();
#endif
