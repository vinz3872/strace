/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/17 13:05:49 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 15:55:10 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

t_data	*getData() {
	static t_data *data =  NULL;

	if (data == NULL) {
		data = malloc(sizeof(t_data));
		assert(data != NULL);
		data->sysLst = NULL;
		data->begin = 0;
		data->pid = -1;
	}
	return (data);
}
