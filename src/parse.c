/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vjacquie <vjacquie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/15 15:19:56 by vjacquie          #+#    #+#             */
/*   Updated: 2016/05/20 18:58:11 by vjacquie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_strace.h>

// check sigset_t
int		isIntType(char *elem) {
	if (ft_strstr(elem, "int ") != NULL)
		return (1);
	else if (ft_strstr(elem, "long ") != NULL)
		return (1);
	else if (ft_strstr(elem, "_t ") != NULL)
		return (1);
	else if (ft_strstr(elem, "u32 ") != NULL)
		return (1);
	else if (ft_strstr(elem, "u64 ") != NULL)
			return (1);
	return (0);
}

void	parseType(char *elem, t_syscall *new, int i) {
	if (ft_strstr(elem, "char *") != NULL)
		new->type[i] = ARG_STR;
	else if (ft_strstr(elem, "struct") != NULL)
		new->type[i] = ARG_PTR;
	else if (isIntType(elem) == 1)
		new->type[i] = ARG_INT;
	else
		new->type[i] = ARG_PTR;
}

void     parseLine(char *line) {
	t_syscall *new = newLst();
	char **arr = NULL;
	int i = 0;

	if (line[ft_strlen(line) - 1] == '\n')
		line[ft_strlen(line) - 1] = '\0';
	arr = ft_strsplit(line, '\t');
	while (arr[i] && ft_strlen(arr[i]) >= 1) {
		if (i == 0)
			new->sysNumber = ft_atoi(arr[i]);
		else if (i == 1)
			new->name = ft_strdup(arr[i] + 4);
		else
			parseType(arr[i], new, i - 2);
		i++;
	}
	assert(i >= 2);
	new->argNbr = i - 2;
	addLst(new);
}

int     parseFile() {
	FILE    *fd;
	char    *line = NULL;
	size_t  len = 0;

	if ((fd = fopen(SYSCALLPATH, "r")) == NULL)
		return (print_error("parseFile : fopen failed"));

	while (getline(&line, &len, fd) != -1) {
		if (line[0] == '#')
			continue;
		else
			parseLine(line);
	}
	if (line)
		free(line);
	fclose(fd);
	return (1);
}
